<?php
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json; charset-UTF-8");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');

//Serve para ler todo o post gerado por uma pagina que chamou o arquivo
$data = file_get_contents("php://input");

// Decodifica uma string JSON
$objData = json_decode($data);

$nome       = $objData->nome;
$email      = $objData->email;
$senha      = $objData->senha;
$dataHora	= date('Y-m-d G:i:s');
$status		= "ativo";
$foto		= "cliente/cliente.png";

//stripslashes: Remova a barra invertida
$nome       = stripslashes($nome);
$email      = stripslashes($email);
$senha      = stripslashes($senha);

//trim: Remove espaços em branco
$nome       = trim($nome);
$email      = trim($email);
$senha      = trim($senha);

$dados; 

require_once("class-conexao.php");
			
$conexao = Conexao::LigarConexao();
$conexao->exec("SET NAMES utf8");

if($conexao){

    $Sql = "insert into cliente (nomeCli, emailCli, senhaCli, statusCli, dataCadCli, fotoCli) values('".$nome."','".$email."','".$senha."', '".$status."', '".$dataHora."', '".$foto."')";
	
    $query = $conexao->prepare($Sql);
    $query->execute();

	$dados = array('mensage' => "Dados inseridos com sucesso");
  	echo json_encode($dados);

}else{
	$dados = array('mensage' => "Não foi possivel realizar o cadastro! Tente novamente mais tarde.");
	echo json_encode($dados);
};
?>