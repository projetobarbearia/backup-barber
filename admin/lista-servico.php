<?php
    require_once("class-conexao.php");
    try{
        $conexao = Conexao::LigarConexao();
        $conexao->exec("SET NAMES utf8");

        if(!$conexao){
            echo "Não foi possivel conectar ao banco de dados";
        }

        $query = $conexao->prepare("SELECT * FROM servico ORDER BY idservico ASC");
        $query->execute();
		
        $json = "[";
		
        while ($resultado = $query->fetch()) {  
            
            if($json != "["){
                $json .= ",";
            }
			
            $json .= '{"codigo":               "'.$resultado["idservico"].'",';
            $json .= '"nome":                  "'.$resultado["nomeServico"].'",';
            $json .= '"valor":                 "'.$resultado["valorServico"].'",';
            $json .= '"status":                "'.$resultado["statusServico"].'",';
            $json .= '"dataCadastro":          "'.$resultado["dataCadServico"].'",';
            $json .= '"foto1":                 "'.$resultado["fotoServico1"].'",';
            $json .= '"foto2":                 "'.$resultado["fotoServico2"].'",';
            $json .= '"foto3":                 "'.$resultado["fotoServico3"].'",';
            $json .= '"descricao":             "'.$resultado["descServico"].'",';
            $json .= '"tempo":                 "'.$resultado["tempoServico"].'",';
            $json .= '"empresa":               "'.$resultado["idempresa"].'"}';
			
        }
        $json .= "]";
        echo $json;
    }catch(Exeption $e){
        echo "Erro! ". $e->getMessage();
    }
?>