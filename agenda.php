<!DOCTYPE html>
<html lang="pt-br">
    <head>
   <meta charset="utf-8">
   <title> Barbearia Caravelas</title>
   
   <!--CSS de reset das configurações do browser-->
   <link rel="stylesheet" type="text/css" href="css/reset.css">
   <link rel="icon" type="image/png" sizes="32x32" href ="img/icon/lg.png">
   <link rel="stylesheet" type="text/css" href="css/slick.css">
   <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
   <link rel="stylesheet" type="text/css" href="css/lity.css">
   <link rel="stylesheet" type="text/css" href="css/animate.css">
   <link rel="stylesheet" type="text/css" href="css/animate1.css">
   <link rel="stylesheet" type="text/css" href="css/agenda.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
   <script type="text/javascript">
   jQuery(document).ready(function(){
   
   jQuery("#descer").hide();
   
   jQuery('a#descer').click(function () {
            jQuery('body,html').animate({
              scrollTop: 0
            }, 800);
           return false;
      });
   
   jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 1000) {
               jQuery('#descer').fadeIn();
            } else {
               jQuery('#descer').fadeOut();
            }
        });
   });
   </script>
   </head>
   

</div>
    <body>  
            <!--CABEÇALHO DO SITE-->
        <header id="topo-fixo">     
                <!-- <div class="logo">
                        <img src="img/fff.png" alt="logo" width="800" height="800" />
                    </div>                   -->
                <div class="site topo"> 
                        <h1> Barbearia Caravelas</h1>      
                     <nav>      
                        <ul><!--Lista de marcadores-->
                            <li>
                                <a href="index.php">
                                    <span class="icon iconInicio"></span>
                                        <span>INICIO</span>        
                                </a>
                            </li><!--Fim item da lista-->
                            <li>
                                <a href="indexServico.php">
                                    <span class="icon iconServico"></span>
                                    <span>UNIDADES</span>
                                </a>
                            </li>
                            <div class="logo">
                                    <img src="img/fff.png" alt="logo" width="120" height="120"  />
                                </div>  
                            <li>
                                <a href="#barbeiro">
                                    <span class="icon iconGaleria"></span>
                                    <span>BARBEIROS</span>
                                </a> 
                                

                            </li>
                            <li>
                                <a href="indexCont.php">
                                    <span class="icon iconContato"></span>
                                    <span>CONTATO</span>
                                </a>

                            </li>

                       </ul>            
                    </nav>
                </div>                   
        </header>     
      <div class="agendamento">
            <img src="img/bannerApp.png" width="1900">
        </div>
        
        <div class="barbeiros" id="barbeiro">
            <img src="img/BARBEIROS1.png" width="1900">
            <section class="banner1"><!--BANNER-->              
                <img src="img/BETO.png" alt="Banner Site KiBeleza">
                <img src="img/GILBERTO.png" alt="Banner Site KiBeleza">
                <img src="img/TREVOR.png" alt="Banner Site KiBeleza">
            
                <img src="img/HENRIQUE.png" alt="Banner Site KiBeleza">
                <img src="img/EDUARDO.png" alt="Banner Site KiBeleza">
                <img src="img/WILLY.png" alt="Banner Site KiBeleza">
                <!-- <img src="img/banner3.jpg" alt="Banner Site KiBeleza">
                <img src="img/banner4.jpg" alt="Banner Site KiBeleza"> -->
                </section>
               
                <div class="unidades">
                    <img src="img/unidades2.png">
                </div>
        </div>
        <footer class="rodape">
                <p>Karla e Luccas - Todos os direitos reservados</p>
            </footer>
        </body>
        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/slick.js"></script>
        <script type="text/javascript" src="js/wow.js"></script>
        <script type="text/javascript" src="js/lity.js"></script>
        <script type="text/javascript" src="js/animacao.js"></script>
        <script src="https://cdn.shopify.com/s/files/1/0683/5883/t/3/assets/instafeed.min.js"></script>

        <script src="https://cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>

        <script src="js/instafeed.js"></script>
        </html>
        