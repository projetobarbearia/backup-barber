<footer class="rodape wow fadeInUpBig"><!--INICIO RODAPE-->
    <div class="site4">
        <img src="img/logosvg.svg" alt="Logo Caravela">
        <p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI</p>
        <ul class="icones-redes-sociais"> 
            <li>
                <a class="facebook" href="https://www.facebook.com/" target="_blank">
                Facebook
                </a>
            </li>
            <li>
                <a class="twitter" href="https://www.twitter.com/" target="_blank">
                Twitter
                </a>
            </li>
            <li>
                <a class="instagram" href="https://www.instagram.com/" target="_blank">
                Instagram
                </a>
            </li>
        </ul>
    </div>
 <!-- </div> -->
</section>    
<a id="subirTopo">
   <img src="img/subir.png" width="5" height="5">
  </a>
 </footer><!--FIM INICIO RODAPE-->
 <h4 class="direitos">&copy;Todos os direitos reservados Barbearia Caravelas</h4>

 