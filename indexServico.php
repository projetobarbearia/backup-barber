<!DOCTYPE html>
<html lang="pt-br">
    <head>
   <meta charset="utf-8">
   <title> Barbearia Caravelas</title>
   <!--CSS de reset das configurações do browser-->
   <link rel="stylesheet" type="text/css" href="css/reset.css">
   <link rel="icon" type="image/png" sizes="32x32" href ="img/icon/lg.png">
   <link rel="stylesheet" type="text/css" href="css/slick.css">
   <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
   <link rel="stylesheet" type="text/css" href="css/lity.css">
   <link rel="stylesheet" type="text/css" href="css/animate.css">
   <link rel="stylesheet" type="text/css" href="css/estiloServico.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
   <script type="text/javascript">
   jQuery(document).ready(function(){
   
   jQuery("#subirTopo").hide();
   
   jQuery('a#subirTopo').click(function () {
            jQuery('body,html').animate({
              scrollTop: 0
            }, 800);
           return false;
      });
   
   jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 1000) {
               jQuery('#subirTopo').fadeIn();
            } else {
               jQuery('#subirTopo').fadeOut();
            }
        });
   });
   </script>
   </head>
</div>
    <body>  
            <!--CABEÇALHO DO SITE-->
        <header id="topo-fixo">     
                <!-- <div class="logo">
                        <img src="img/fff.png" alt="logo" width="480" height="480"  />
                    </div>                   -->
                <div class="site topo"> 
                        <h1> Barbearia Caravelas</h1>      
                     <nav>      
                        <ul><!--Lista de marcadores-->
                            <li>
                                <a href="index.php">
                                    <span class="icon iconInicio"></span>
                                        <span>INICIO</span>        
                                </a>
                            </li><!--Fim item da lista-->
                            <li>
                                <a href="indexServico.php">
                                    <span class="icon iconServico"></span>
                                    <span>SERVIÇOS</span>
                                </a>
                            </li>
                            <div class="logo">
                                    <img src="img/fff.png" alt="logo" width="120" height="120"  />
                                </div>  
                            <li>
                                <a href="indexGaleria.php">
                                    <span class="icon iconGaleria"></span>
                                    <span>GALERIA</span>
                                </a> 
                            </li>
                            <li>
                                <a href="indexCont.php">
                                    <span class="icon iconContato"></span>
                                    <span>CONTATO</span>
                                </a>

                            </li>

                       </ul>            
                    </nav>
                </div>                   
        </header>     
      
      
        <div class="fotos">
            <img src="img/fotos.png" alt="Fotos">
        </div>

        <div class="tabela">
            <img src="img/tabela1.png" alt="tabela de servico">
            <!-- <img src="img/texto.png" alt="texto tabela"> -->
        </div>
        <div class="texto">
                <img src="img/texto.png" alt="texto tabela">
        </div>
        <br>
        <div class="historia">
        <img src="img/historia.png" width="1900" height="600">
        </div>
        <div class="fle">
            <img src="img/fle.png">
                </div>
                <br>
              
        <div class="noivo">
            <img src="img/noivo.png">
        </div>
        <section class="galeria"><!--GALERIA INSTA-->
            <div id="instafeed" class="instafeed"></div>
        </section><!--FIM GALERIA INSTA-->
        <div class="insta">
            <img src="img/insta20.png" width="130" height="130">
        </div>

        <footer class="rodape"><!--INICIO RODAPE-->
            <div class="site4">
                <img src="img/logosvg.svg" alt="Logo Caravela">
                <p>Barba, cabelo & bigode</p>
                <ul class="icones-redes-sociais"> 
                    <li>
                        <a class="facebook" href="https://www.facebook.com/" target="_blank">
                        Facebook
                        </a>
                    </li>
                    <li>
                        <a class="twitter" href="https://www.twitter.com/" target="_blank">
                        Twitter
                        </a>
                    </li>
                    <li>
                        <a class="instagram" href="https://www.instagram.com/" target="_blank">
                        Instagram
                        </a>
                    </li>
                </ul>
            </div>
            <a id="subirTopo">
                <img src="img/subir.png" width="5" height="5">
               </a>
        </footer><!--FIM INICIO RODAPE-->
        <h4 class="direitos">&copy;Todos os direitos reservados Barbearia Caravelas</h4> 
  
</body>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/slick.js"></script>
<script type="text/javascript" src="js/wow.js"></script>
<script type="text/javascript" src="js/lity.js"></script>
<script type="text/javascript" src="js/animacao.js"></script>
<script src="https://cdn.shopify.com/s/files/1/0683/5883/t/3/assets/instafeed.min.js"></script>

<script src="https://cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>

<script src="js/instafeed.js"></script>
</html>