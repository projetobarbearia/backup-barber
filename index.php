<!DOCTYPE html>
<html lang="pt-br">
    <head>
   <meta charset="utf-8">
   <title> Barbearia Caravelas</title>
   
   <!--CSS de reset das configurações do browser-->
   <link rel="stylesheet" type="text/css" href="css/reset.css">
   <meta name="viewport" content="width=device-width">
   <link rel="icon" type="image/png" sizes="32x32" href ="img/icon/lg.png">
   <link rel="stylesheet" type="text/css" href="css/slick.css">
   <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
   <link rel="stylesheet" type="text/css" href="css/lity.css">
   <link rel="stylesheet" type="text/css" href="css/animate.css">
   <link rel="stylesheet" type="text/css" href="css/animate1.css">
   <link rel="stylesheet" type="text/css" href="css/estilo.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
   <script type="text/javascript">
   jQuery(document).ready(function(){
   
   jQuery("#subirTopo").hide();
   
   jQuery('a#subirTopo').click(function () {
            jQuery('body,html').animate({
              scrollTop: 0
            }, 800);
           return false;
      });
   
   jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 1000) {
               jQuery('#subirTopo').fadeIn();
            } else {
               jQuery('#subirTopo').fadeOut();
            }
        });
   });
   </script>
   </head>
   

</div>
    <body>  
            <!--CABEÇALHO DO SITE-->
        <header id="topo-fixo">     
                <!-- <div class="logo">
                        <img src="img/fff.png" alt="logo" width="800" height="800" />
                    </div>                   -->
                    <div class="agenda">
                        <!-- <img src="img/cats.png"> -->
                        <a href="agenda.php">
                        <img src="img/BOTAOAGENDAMENTO.png">
                        </a>
                    </div>
                <div class="site topo"> 
                        <h1> Barbearia Caravelas</h1>      
                     <nav>      
                        <ul><!--Lista de marcadores-->
                            <li>
                                <a href="index.php">
                                    <span class="icon iconInicio"></span>
                                        <span>INICIO</span>        
                                </a>
                            </li><!--Fim item da lista-->
                            <li>
                                <a href="indexServico.php">
                                    <span class="icon iconServico"></span>
                                    <span>SERVIÇOS</span>
                                </a>
                            </li>
                            <div class="logo">
                                    <img src="img/fff.png" alt="logo">
                                </div>  
                            <li>
                                <a href="indexGaleria.php">
                                    <span class="icon iconGaleria"></span>
                                    <span>GALERIA</span>
                                </a> 
                            </li>
                            <li>
                                <a href="indexCont.php">
                                    <span class="icon iconContato"></span>
                                    <span>CONTATO</span>
                                </a>

                            </li>

                       </ul>            
                    </nav>

                    

                </div>                   
        </header>     
      
        <video autoplay loop poster="video.jpg" class="bg_video">
            <source src="videos/videocaravela.mp4" type="video/mp4">
        </video>
        <!-- <section class="servico wow fadeInUpBig">DESTAQUE -->
                <div class="ss">
                        <img src="img/quem2.png">   
                    </div>
                    <section class="ss1 wow bounceInLeft">
                    <div class="ss1">
                        <img src="img/quemPart1.png">
                    </div>
                    </section>
                    <section class="ss2 wow bounceInRight">
                    <div class="ss2">
                        <img src="img/quemPart2.png">
                    </div>
                    </section>
            
                <div class="unidades">
                        <img src="img/unidades2.png">
                    </div>

                <section class="time wow fadeInDown">
               <div class="time">
                   <img src="img/time.png" width="300" height="150">
                   <a href="animate1.css"></a>
               </div>
                </section>
                
          
                
                <div class="barbeiros">
                    <img src="img/BARBEIROS1.png" width="1900">
                    <section class="banner1"><!--BANNER-->              
                        <img src="img/BETO.png" alt="Banner Site KiBeleza">
                        <img src="img/GILBERTO.png" alt="Banner Site KiBeleza">
                        <img src="img/TREVOR.png" alt="Banner Site KiBeleza">
                       
                        <img src="img/HENRIQUE.png" alt="Banner Site KiBeleza">
                        <img src="img/EDUARDO.png" alt="Banner Site KiBeleza">
                        <img src="img/WILLY.png" alt="Banner Site KiBeleza">
                        <!-- <img src="img/banner3.jpg" alt="Banner Site KiBeleza">
                        <img src="img/banner4.jpg" alt="Banner Site KiBeleza"> -->
                        </section>
           <section class="app wow fadeInLeft">
           <div class="app">          
            <img src="img/iphone1.png" > 
            <a href="animate1.css"></a>      
            </div>
           </section>
           <div class="happ">
               <img src="img/app1.png">
           </div>
            <section class="baixar wow fadeInUpBig">
            <div class="apple">
                 <a href="https://www.apple.com/br/ios/app-store/" target="_blank">
                     <img src="img/appple.png" width="150">
                     </a>
            </div>
            <div class="pstore">
                 <a href="https://play.google.com/store/apps?hl=pt_BR" target="_blank">
                     <img src="img/google.png" width="150">
                     </a>
            </div>
        </section>
        <!-- <section class="sobre wow fadeInUpBig"> SOBRE -->
            <!-- <div class="ss">
                <img src="img/quem.png" width="1900" height="820">   
            </div> -->
            <!-- <div class="botaoleia">
                <a href="https://www.youtube.com/watch?v=EzBnDmest_8">
                <img src="img/leiamais.png" alt="img do leia">
                </a>  
            </div> -->
        </section> <!--FIM SOBRE-->
     


     
            <div class="serv">
                    <img src="img/ss.png" width="1900" height="602" >    
                </div>
            <div class="botaoservi">
                <a href="/indexServico.html">
                <img src="img/tdsservicos.png" alt="img do serv">
                </a>  
            </div>
        </section> <!--FIM SOBRE-->
        <section class="galeria wow fadeInUnBig"><!--GALERIA INSTA-->
            <div id="instafeed" class="instafeed"></div>
        </section><!--FIM GALERIA INSTA-->
        <div class="insta">                
                <a class="Instagram" href="https://www.instagram.com/barbeariacaravelas01/" target="_blank">
                   
            </div>
			
			<div class="ins">
			<img src="img/insta22.png" width="530" height="530"></a>
			</div>
        

<footer class="rodape"><!--INICIO RODAPE-->
    <div class="site4">
        <img src="img/logosvg.svg" alt="Logo Caravela">
        <p>Barba, cabelo & bigode</p>
        <ul class="icones-redes-sociais"> 
            <li>
                <a class="facebook" href="https://www.facebook.com/" target="_blank">
                Facebook
                </a>
            </li>
            <li>
                <a class="twitter" href="https://www.twitter.com/" target="_blank">
                Twitter
                </a>
            </li>
            <li>
                <a class="instagram" href="https://www.instagram.com/" target="_blank">
                Instagram
                </a>
            </li>
        </ul>
    </div>
 <!-- </div> -->
</section>    
<a id="subirTopo">
   <img src="img/subir.png" width="5" height="5">
  </a>
 </footer><!--FIM INICIO RODAPE-->
 <h4 class="direitos">&copy;Todos os direitos reservados Barbearia Caravelas</h4>

 
        
     </body>
        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/slick.js"></script>
        <script type="text/javascript" src="js/wow.js"></script>
        <script type="text/javascript" src="js/lity.js"></script>
        <script type="text/javascript" src="js/animacao.js"></script>
        <script src="https://cdn.shopify.com/s/files/1/0683/5883/t/3/assets/instafeed.min.js"></script>

        <script src="https://cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>

        <script src="js/instafeed.js"></script>
        
</html>